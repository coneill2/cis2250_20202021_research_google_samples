package info.hccis.camper.rest;

import com.google.gson.Gson;
import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.repositories.CamperRepository;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/CamperService/campers")
public class CamperService {

    private final CamperRepository cr;

    @Autowired
    public CamperService(CamperRepository cr) {
        this.cr = cr;
    }

    @GET
    @Produces("application/json")
    public ArrayList<Camper> getAll() {
        ArrayList<Camper> campers = (ArrayList<Camper>) cr.findAll();
        return campers;
    }

    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getCamperById(@PathParam("id") int id) throws URISyntaxException {

        Optional<Camper> camper = cr.findById(id);

        if (!camper.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(camper).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCamper(String camperJson) 
    {        
        try{
            String temp = save(camperJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
//    
//    @DELETE
//    @Path("/{id}")
//    public Response deleteBooking(@PathParam("id") int id) throws URISyntaxException {
//        Optional<Booking> booking = br.findById(id);
//        if(booking != null) {
//            br.deleteById(id);
//            return Response.status(HttpURLConnection.HTTP_CREATED).build();
//        }
//        return Response.status(404).build();
//    }
//
    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateCamper(@PathParam("id") int id, String camperJson) throws URISyntaxException 
    {

        try{
            String temp = save(camperJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }

    }

    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        Camper camper = gson.fromJson(json, Camper.class);
        
        if(camper.getFirstName() == null || camper.getFirstName().isEmpty()) {
            throw new AllAttributesNeededException("Please provide all mandatory inputs");
        }
 
        if(camper.getId() == null){
            camper.setId(0);
        }

        camper = cr.save(camper);

        String temp = "";
        temp = gson.toJson(camper);

        return temp;
        
        
    }
    
}
