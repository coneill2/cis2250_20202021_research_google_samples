package info.hccis.camper.bo;

import info.hccis.camper.dao.CamperDAO;
import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.repositories.CamperRepository;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Camper business object
 *
 * @author CIS2232
 * @since 20200923
 */
public class CamperBO {

    public static ArrayList<Camper> loadCampers(CamperRepository cr) {
        ArrayList<Camper> campers = (ArrayList<Camper>) cr.findAll();
        HashMap<Integer, CamperType> camperTypesMap = CamperTypeBO.getCamperTypesMap();
        for (Camper current : campers) {
            if (current.getCamperType() != null) {
                current.setCamperTypeDescription(camperTypesMap.get(current.getCamperType()).getDescription());
            }
        }
        return campers;

    }

    /**
     * Connect to the data access object to get the campers from the datasource.
     *
     * @since 20200923
     * @author BJM
     */
    public ArrayList<Camper> load() {

        //Read campers from the database
        CamperDAO camperDAO = new CamperDAO();
        ArrayList<Camper> campers = camperDAO.select();

        return campers;
    }

    public Camper save(Camper camper) {
        CamperDAO camperDAO = new CamperDAO();

        //NOTE:  The id attribute generated from the database is an Integer not an
        //       int.  The default for an Integer is null so can't compare to 0.
        if (camper.getId() == null) {
            camper = camperDAO.insert(camper);
        } else {
            camperDAO.update(camper);
        }

        return camper;
    }

    /**
     * Delete the camper
     *
     * @since 20201009
     * @author BJM
     */
    public boolean delete(int id) {
        CamperDAO camperDAO = new CamperDAO();
        return camperDAO.delete(id);
    }

}
