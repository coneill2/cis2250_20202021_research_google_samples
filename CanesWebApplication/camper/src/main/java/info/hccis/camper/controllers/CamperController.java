package info.hccis.camper.controllers;

import info.hccis.camper.bo.CamperBO;
import info.hccis.camper.jpa.entity.Camper;
import info.hccis.camper.jpa.entity.CamperType;
import info.hccis.camper.repositories.CamperRepository;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Controller for the camper functionality of the site
 *
 * @since 20200930
 * @author CIS2232
 */
@Controller
@RequestMapping("/camper")
public class CamperController {

    private final CamperRepository camperRepository;

    public CamperController(CamperRepository cr) {
        camperRepository = cr;
    }

    /**
     * Page to allow user to view campers
     *
     * @since 20200528
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the campers from the database.
        //Use the jpa repository to get the campers.
        model.addAttribute("campers", CamperBO.loadCampers(camperRepository));
        model.addAttribute("findNameMessage", "Campers loaded");

        return "camper/list";
    }

    /**
     * Page to allow user to add a camper
     *
     * @since 20201002
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/add")
    public String add(Model model, HttpSession session) {

        HashMap<Integer, CamperType> camperTypesMap = (HashMap<Integer, CamperType>) session.getAttribute("camperTypesMap");
        String camperType1Description = camperTypesMap.get(1).getDescription();

        model.addAttribute("message", "Add a camper");

        Camper camper = new Camper();
        model.addAttribute("camper", camper);

        return "camper/add";
    }

    /**
     * Page to allow user to submit the add a camper. It will put the camper in
     * the database.
     *
     * @since 20201002
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("camper") Camper camper, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "camper/add";
        }

        //Save that camper to the database
//        CamperBO camperBO = new CamperBO();
//        camperBO.save(camper);
        camperRepository.save(camper);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successAddString = rb.getString("message.camper.saved");

        model.addAttribute("message", successAddString);
        model.addAttribute("campers", CamperBO.loadCampers(camperRepository));

        return "camper/list";

    }

    /**
     * Page to allow user to edit a camper
     *
     * @since 20201007
     * @author BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

//        //reload the list
//        
//        ArrayList<Camper> campers = (ArrayList<Camper>) camperRepository.findAll();
        //TODO use findOne here.,
        Optional<Camper> found = camperRepository.findById(id);
//        for (Camper current : campers) {
//            if (current.getId() == id) {
//                found = current;
//                break;
//            }
//        }

        model.addAttribute("camper", found);
        return "camper/add";
    }

    /**
     * Page to allow user to delete a camper
     *
     * @since 20201009
     * @author BJM
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        String propFileName = "messages";
        ResourceBundle rb = ResourceBundle.getBundle(propFileName);
        String successString;
        try {
            camperRepository.deleteById(id);
            successString = rb.getString("message.camper.deleted") + " (" + id + ")";
        } catch (EmptyResultDataAccessException e) {
            successString = rb.getString("message.camper.deleted.error") + " (" + id + ")";
        }

        model.addAttribute("message", successString);
        model.addAttribute("campers", CamperBO.loadCampers(camperRepository));

        return "camper/list";
    }


}
