/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.hccis.cis2250.canescamper.ui.darkTheme;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;

public class ThemeHelper {

    //Constants that match the themeEntryArray values populating the theme list options
    public static final String LIGHT_MODE = "light";
    public static final String DARK_MODE = "dark";
    public static final String DEFAULT_MODE = "default";

    public static void applyTheme(@NonNull String themePref) {
        /*
        Use a switch to evaluate the theme preference
        - passed from the DarkThemeApplication (shared preferences storing the user's choice)
        - passed from SettingsFragment onPreferenceChangeListener

        AppCompatDelegate.setDefaultNightMode method determines what theme is used
        - MODE_NIGHT_NO. Always light theme
        - MODE_NIGHT_YES. Always dark theme
        - MODE_NIGHT_FOLLOW_SYSTEM. Follows current system setting (SDK 29+)
        - MODE_NIGHT_AUTO_BATTERY. Dark when battery saver is enabled, light otherwise
        */
        switch (themePref) {
            case LIGHT_MODE: {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
            }
            case DARK_MODE: {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;
            }
            default: {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                } else {
                    //Used before SDK 29 (Android P or earlier) when a system wide setting was unavailable
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY);
                }
                break;
            }
        }
    }
}
