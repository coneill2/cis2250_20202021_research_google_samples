/*
 * Copyright (C) 2019 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.hccis.cis2250.canescamper.ui.darkTheme;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

public class DarkThemeApplication extends Application {

    public void onCreate() {
        super.onCreate();
        /*
        Google recommends storing the user's choice so that it is applied when the app is opened again.
        This is achieved using Shared Preferences and the androidX preference library (SettingsFragment.java and preferences.xml files).
        Here the saved user's choice is retrieved and applied by the ThemeHelper when the user opens the app.
         */
        SharedPreferences sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(this);
        String themePref = sharedPreferences.getString("themePref", ThemeHelper.DEFAULT_MODE);
        ThemeHelper.applyTheme(themePref);
    }
}
